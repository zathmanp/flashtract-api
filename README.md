# Flashtract API

### Quick Reference

This application was developed using:

* Java 11
* Apache Maven
* Spring Boot 2.4.1
* Flyway
* Spring Data JPA
* Swagger
* H2 Database
* Lombok
* ModelMapper

### URLs

The base URL is: http://localhost:8080/flashtract/api 

The project is configured to be accessed from 2 additional URLs:

* [H2 Console](http://localhost:8080/flashtract/api/h2-console/) (username: flashtract / password:
  flash)
* [Swagger API Documentation](http://localhost:8080/flashtract/api/swagger-ui/)