CREATE TABLE user
(
    id         VARCHAR(50) PRIMARY KEY,
    name       VARCHAR(50) NOT NULL,
    last_name  VARCHAR(50) NOT NULL,
    type       VARCHAR(20) NOT NULL,
    created_at DATETIME    NOT NULL,
    updated_at DATETIME
);

CREATE TABLE contract
(
    id          VARCHAR(50) PRIMARY KEY,
    description VARCHAR(200) NOT NULL,
    amount      NUMBER       NOT NULL,
    status      VARCHAR(20)  NOT NULL DEFAULT 'APPROVED',
    created_at  DATETIME     NOT NULL,
    updated_at  DATETIME,
    client_id   VARCHAR(50)  NOT NULL,
    vendor_id   VARCHAR(50)  NOT NULL,
    FOREIGN KEY (client_id) REFERENCES user (id),
    FOREIGN KEY (vendor_id) REFERENCES user (id)
);

CREATE TABLE invoice
(
    id          VARCHAR(50) PRIMARY KEY,
    description VARCHAR(50) NOT NULL,
    amount      NUMBER      NOT NULL,
    status      VARCHAR(20) NOT NULL DEFAULT 'APPROVED',
    created_at  DATETIME    NOT NULL,
    updated_at  DATETIME,
    contract_id VARCHAR(50) NOT NULL,
    FOREIGN KEY (contract_id) REFERENCES contract (id)
);