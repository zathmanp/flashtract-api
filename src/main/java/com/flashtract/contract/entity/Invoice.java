package com.flashtract.contract.entity;

import com.flashtract.contract.enums.StatusEnum;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Getter
@Setter
public class Invoice {

    @Id
    @GeneratedValue(generator = "INVOICE_UUID")
    @GenericGenerator(
        name = "INVOICE_UUID",
        strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false)
    private String id;
    private String description;
    private Long amount;
    @Column(insertable = false, nullable = false)
    @Enumerated(EnumType.STRING)
    private StatusEnum status;
    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    // Associations
    @ManyToOne
    private Contract contract;

}
