package com.flashtract.contract.repository;

import com.flashtract.contract.entity.Contract;
import com.flashtract.contract.entity.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, String> {

    @Query("SELECT COALESCE(SUM(i.amount), 0) FROM Invoice i WHERE i.contract = ?1")
    long getInvoicedContractAmount(Contract contract);

}
