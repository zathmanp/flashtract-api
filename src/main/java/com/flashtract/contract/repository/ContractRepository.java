package com.flashtract.contract.repository;

import com.flashtract.contract.entity.Contract;
import com.flashtract.contract.entity.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractRepository extends JpaRepository<Contract, String> {

    Contract findByIdAndClient(String id, User client);

    Contract findByIdAndVendor(String id, User vendor);

    List<Contract> findAllByClient(User client);

    List<Contract> findAllByVendor(User vendor);

}
