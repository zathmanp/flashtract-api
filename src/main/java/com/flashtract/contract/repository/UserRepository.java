package com.flashtract.contract.repository;

import com.flashtract.contract.entity.User;
import com.flashtract.contract.enums.UserTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    boolean existsByIdAndType(String id, UserTypeEnum type);

}
