package com.flashtract.contract.exception;

/**
 * Exception intended to be thrown when a contract's amount has been exceeded
 */
public class ContractAmountExceededException extends RuntimeException {

    public ContractAmountExceededException(String message) {
        super(message);
    }

}
