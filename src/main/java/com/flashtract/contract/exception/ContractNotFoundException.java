package com.flashtract.contract.exception;

/**
 * Exception intended to be thrown when a contract is not found
 */
public class ContractNotFoundException extends RuntimeException {

    public ContractNotFoundException(String message) {
        super(message);
    }

}
