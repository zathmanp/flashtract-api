package com.flashtract.contract.exception;

/**
 * Exception intended to be thrown when a user is not found
 */
public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String message) {
        super(message);
    }

}
