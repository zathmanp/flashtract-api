package com.flashtract.contract.exception;

/**
 * Exception intended to be thrown when a contract has been already paid
 */
public class ContractPaidException extends RuntimeException {

    public ContractPaidException(String message) {
        super(message);
    }

}
