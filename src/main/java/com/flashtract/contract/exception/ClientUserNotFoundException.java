package com.flashtract.contract.exception;

/**
 * Exception intended to be thrown when a user (Type CLIENT) is not found
 */
public class ClientUserNotFoundException extends UserNotFoundException {

    public ClientUserNotFoundException(String message) {
        super(message);
    }

}
