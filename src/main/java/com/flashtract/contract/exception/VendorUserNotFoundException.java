package com.flashtract.contract.exception;

/**
 * Exception intended to be thrown when a user (Type VENDOR) is not found
 */
public class VendorUserNotFoundException extends UserNotFoundException {

    public VendorUserNotFoundException(String message) {
        super(message);
    }

}
