package com.flashtract.contract.converter;

import com.flashtract.contract.controller.response.ContractResponse;
import com.flashtract.contract.controller.response.InvoiceResponse;
import com.flashtract.contract.entity.Contract;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;

/**
 * Custom converter from {@link Contract} entity to {@link ContractResponse}
 */
@RequiredArgsConstructor
public class ContractConverter implements Converter<Contract, ContractResponse> {

    private final ModelMapper modelMapper;

    @Override
    public ContractResponse convert(MappingContext<Contract, ContractResponse> mappingContext) {
        Contract contract = mappingContext.getSource();
        ContractResponse response = new ContractResponse();
        response.setId(contract.getId());
        response.setDescription(contract.getDescription());
        response.setAmount(contract.getAmount());
        response.setStatus(contract.getStatus().name());
        response.setCreatedAt(contract.getCreatedAt());

        List<InvoiceResponse> invoices = new ArrayList<>();
        if (contract.getInvoices() != null && !contract.getInvoices().isEmpty()) {
            contract.getInvoices().stream().map(i -> invoices.add(modelMapper.map(i, InvoiceResponse.class)))
                .collect(Collectors.toList());
        }
        response.setInvoices(invoices);

        return response;
    }

}
