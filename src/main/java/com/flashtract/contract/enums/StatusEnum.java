package com.flashtract.contract.enums;

/**
 * Available contract / invoice statuses
 */
public enum StatusEnum {

    IN_PROGRESS,
    SUBMITTED,
    APPROVED

}
