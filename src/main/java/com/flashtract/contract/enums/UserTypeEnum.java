package com.flashtract.contract.enums;

/**
 * Available user types
 */
public enum UserTypeEnum {

    CLIENT,
    VENDOR

}
