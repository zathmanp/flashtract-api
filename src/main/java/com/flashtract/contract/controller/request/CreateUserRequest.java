package com.flashtract.contract.controller.request;

import com.flashtract.contract.enums.UserTypeEnum;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateUserRequest {

    @NotBlank(message = "Name is mandatory")
    private String name;
    @NotBlank(message = "Last name is mandatory")
    private String lastName;
    @NotNull
    private UserTypeEnum type;

}
