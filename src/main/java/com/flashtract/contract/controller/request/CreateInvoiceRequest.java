package com.flashtract.contract.controller.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateInvoiceRequest {

    @NotBlank(message = "Description is mandatory")
    private String description;
    @NotNull
    @Min(value = 1, message = "Minimum amount value is 1")
    private Long amount;

}
