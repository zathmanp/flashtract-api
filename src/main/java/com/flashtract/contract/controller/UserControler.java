package com.flashtract.contract.controller;

import com.flashtract.contract.controller.request.CreateContractRequest;
import com.flashtract.contract.controller.request.CreateUserRequest;
import com.flashtract.contract.controller.response.CreateContractResponse;
import com.flashtract.contract.controller.response.CreateUserResponse;
import com.flashtract.contract.exception.ClientUserNotFoundException;
import com.flashtract.contract.exception.VendorUserNotFoundException;
import com.flashtract.contract.service.IContractService;
import com.flashtract.contract.service.IUserService;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserControler {

    private final IUserService userService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateUserResponse createUser(@RequestBody @Valid CreateUserRequest request) {
        return userService.create(request);
    }

}
