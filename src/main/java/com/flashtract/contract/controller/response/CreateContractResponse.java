package com.flashtract.contract.controller.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateContractResponse {

    private String id;

}
