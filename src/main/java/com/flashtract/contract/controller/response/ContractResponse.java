package com.flashtract.contract.controller.response;

import java.time.LocalDateTime;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContractResponse {

    private String id;
    private String description;
    private Long amount;
    private String status;
    private LocalDateTime createdAt;
    private List<InvoiceResponse> invoices;

}
