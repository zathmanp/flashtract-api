package com.flashtract.contract.controller.response;

import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InvoiceResponse {

    private String id;
    private String description;
    private Long amount;
    private String status;
    private LocalDateTime createdAt;

}
