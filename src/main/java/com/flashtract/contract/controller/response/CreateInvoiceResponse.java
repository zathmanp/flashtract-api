package com.flashtract.contract.controller.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateInvoiceResponse {

    private String id;
    private Long pendingContractAmount;

}
