package com.flashtract.contract.controller;

import com.flashtract.contract.controller.request.CreateInvoiceRequest;
import com.flashtract.contract.controller.response.ContractResponse;
import com.flashtract.contract.controller.response.CreateInvoiceResponse;
import com.flashtract.contract.enums.UserTypeEnum;
import com.flashtract.contract.exception.ContractAmountExceededException;
import com.flashtract.contract.exception.ContractNotFoundException;
import com.flashtract.contract.exception.ContractPaidException;
import com.flashtract.contract.exception.UserNotFoundException;
import com.flashtract.contract.service.IContractService;
import java.util.List;
import javax.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/vendors")
@RequiredArgsConstructor
public class VendorControler {

    private final IContractService contractService;

    @GetMapping("/{id}/contracts/{contractId}")
    @ResponseStatus(HttpStatus.OK)
    public ContractResponse getContract(@PathVariable("id") @NotBlank String id,
        @PathVariable("contractId") @NotBlank String contractId) {
        try {
            return contractService.getContract(id, contractId, UserTypeEnum.VENDOR);
        } catch (UserNotFoundException | ContractNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @GetMapping("/{id}/contracts")
    @ResponseStatus(HttpStatus.OK)
    public List<ContractResponse> getContracts(@PathVariable("id") @NotBlank String id) {
        try {
            return contractService.getContracts(id, UserTypeEnum.VENDOR);
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @PostMapping("/{id}/contracts/{contractId}/invoices")
    @ResponseStatus(HttpStatus.CREATED)
    public CreateInvoiceResponse createContractInvoice(@PathVariable("id") @NotBlank String id,
        @PathVariable("contractId") @NotBlank String contractId, @RequestBody CreateInvoiceRequest request) {
        try {
            return contractService.createInvoice(id, contractId, request);
        } catch (UserNotFoundException | ContractNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        } catch (ContractAmountExceededException | ContractPaidException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage(), e);
        }
    }

}
