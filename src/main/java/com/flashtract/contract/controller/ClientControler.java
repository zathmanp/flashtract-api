package com.flashtract.contract.controller;

import com.flashtract.contract.controller.request.CreateContractRequest;
import com.flashtract.contract.controller.response.ContractResponse;
import com.flashtract.contract.controller.response.CreateContractResponse;
import com.flashtract.contract.enums.UserTypeEnum;
import com.flashtract.contract.exception.ClientUserNotFoundException;
import com.flashtract.contract.exception.ContractNotFoundException;
import com.flashtract.contract.exception.UserNotFoundException;
import com.flashtract.contract.exception.VendorUserNotFoundException;
import com.flashtract.contract.service.IContractService;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/clients")
@RequiredArgsConstructor
public class ClientControler {

    private final IContractService contractService;

    @PostMapping("/{id}/contracts")
    @ResponseStatus(HttpStatus.CREATED)
    public CreateContractResponse createContract(@PathVariable("id") @NotBlank String clientId,
        @RequestBody @Valid CreateContractRequest request) {
        try {
            return contractService.create(clientId, request);
        } catch (ClientUserNotFoundException | VendorUserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @GetMapping("/{id}/contracts/{contractId}")
    @ResponseStatus(HttpStatus.OK)
    public ContractResponse getContract(@PathVariable("id") @NotBlank String id,
        @PathVariable("contractId") @NotBlank String contractId) {
        try {
            return contractService.getContract(id, contractId, UserTypeEnum.CLIENT);
        } catch (UserNotFoundException | ContractNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @GetMapping("/{id}/contracts")
    @ResponseStatus(HttpStatus.OK)
    public List<ContractResponse> getContracts(@PathVariable("id") @NotBlank String id) {
        try {
            return contractService.getContracts(id, UserTypeEnum.CLIENT);
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

}
