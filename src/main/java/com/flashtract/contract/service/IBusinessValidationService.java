package com.flashtract.contract.service;

public interface IBusinessValidationService {

    /**
     * Checks if a user exists, regardless of its type
     *
     * @param id The user id
     */
    void assertUserExists(final String id);

    /**
     * Checks if a user (Type CLIENT) exists
     *
     * @param id The user id
     */
    void assertClientUser(final String id);

    /**
     * Checks if a user (Type VENDOR) exists
     *
     * @param id The user id
     */
    void assertVendorUser(final String id);

    /**
     * Checks if a contract exits
     *
     * @param id The contract id
     */
    void assertContractExists(final String id);

}
