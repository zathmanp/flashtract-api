package com.flashtract.contract.service;

import com.flashtract.contract.controller.request.CreateContractRequest;
import com.flashtract.contract.controller.request.CreateInvoiceRequest;
import com.flashtract.contract.controller.response.ContractResponse;
import com.flashtract.contract.controller.response.CreateContractResponse;
import com.flashtract.contract.controller.response.CreateInvoiceResponse;
import com.flashtract.contract.enums.UserTypeEnum;
import java.util.List;

public interface IContractService {

    /**
     * Creates a new contract
     *
     * @param clientId The id of the user (Type CLIENT) that issues the contract
     * @param request Contract information
     * @return The id of the new contract
     */
    CreateContractResponse create(final String clientId, final CreateContractRequest request);

    /**
     * Gets the information of a single contract
     *
     * @param userId The user id
     * @param contractId The contract id
     * @param userType The user type (CLIENT or VENDOR)
     * @return The contract information
     */
    ContractResponse getContract(final String userId, final String contractId, final UserTypeEnum userType);

    /**
     * Gets a list of contracts for the user
     *
     * @param userId The user id
     * @param userType The user type (CLIENT or VENDOR)
     * @return The contract list
     */
    List<ContractResponse> getContracts(final String userId, final UserTypeEnum userType);

    /**
     * Creates an invoice for a contract
     *
     * @param vendorId The id of the vendor that issues the invoice
     * @param contractId The contract id
     * @param request Invoice information
     * @return The id of the new invoice and the contract's pending amount
     */
    CreateInvoiceResponse createInvoice(final String vendorId, final String contractId,
        final CreateInvoiceRequest request);

}
