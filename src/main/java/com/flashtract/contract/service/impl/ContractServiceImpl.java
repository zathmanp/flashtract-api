package com.flashtract.contract.service.impl;

import com.flashtract.contract.controller.request.CreateContractRequest;
import com.flashtract.contract.controller.request.CreateInvoiceRequest;
import com.flashtract.contract.controller.response.ContractResponse;
import com.flashtract.contract.controller.response.CreateContractResponse;
import com.flashtract.contract.controller.response.CreateInvoiceResponse;
import com.flashtract.contract.entity.Contract;
import com.flashtract.contract.entity.Invoice;
import com.flashtract.contract.entity.User;
import com.flashtract.contract.enums.UserTypeEnum;
import com.flashtract.contract.exception.ContractAmountExceededException;
import com.flashtract.contract.exception.ContractPaidException;
import com.flashtract.contract.repository.ContractRepository;
import com.flashtract.contract.repository.InvoiceRepository;
import com.flashtract.contract.repository.UserRepository;
import com.flashtract.contract.service.IBusinessValidationService;
import com.flashtract.contract.service.IContractService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ContractServiceImpl implements IContractService {

    private final ModelMapper modelMapper;
    private final IBusinessValidationService businessValidationService;
    private final ContractRepository contractRepository;
    private final UserRepository userRepository;
    private final InvoiceRepository invoiceRepository;

    @Override
    public CreateContractResponse create(final String clientId, final CreateContractRequest request) {
        final String vendorId = request.getVendorId();

        businessValidationService.assertClientUser(clientId);
        businessValidationService.assertVendorUser(vendorId);

        User client = userRepository.findById(clientId).get();
        User vendor = userRepository.findById(vendorId).get();

        Contract contract = new Contract();
        contract.setDescription(request.getDescription());
        contract.setAmount(request.getAmount());
        contract.setClient(client);
        contract.setVendor(vendor);
        contract = contractRepository.save(contract);

        CreateContractResponse response = new CreateContractResponse();
        response.setId(contract.getId());

        return response;
    }

    @Override
    @Transactional
    public ContractResponse getContract(final String userId, final String contractId, final UserTypeEnum userType) {
        businessValidationService.assertUserExists(userId);
        businessValidationService.assertContractExists(contractId);

        User user = userRepository.getOne(userId);
        Contract contract = null;

        switch (userType) {
            case CLIENT:
                contract = contractRepository.findByIdAndClient(contractId, user);
                break;
            case VENDOR:
                contract = contractRepository.findByIdAndVendor(contractId, user);
                break;
        }

        return modelMapper.map(contract, ContractResponse.class);
    }

    @Override
    @Transactional
    public List<ContractResponse> getContracts(final String userId, final UserTypeEnum userType) {
        businessValidationService.assertUserExists(userId);
        User user = userRepository.getOne(userId);
        List<Contract> contracts = null;
        List<ContractResponse> responses = new ArrayList<>();
        switch (userType) {
            case CLIENT:
                contracts = contractRepository.findAllByClient(user);
                break;
            case VENDOR:
                contracts = contractRepository.findAllByVendor(user);
                break;
        }

        if (!contracts.isEmpty()) {
            contracts.stream().map(c -> responses.add(modelMapper.map(c, ContractResponse.class)))
                .collect(Collectors.toList());
        }

        return responses;
    }

    @Override
    public CreateInvoiceResponse createInvoice(final String vendorId, final String contractId,
        final CreateInvoiceRequest request) {
        businessValidationService.assertVendorUser(vendorId);
        businessValidationService.assertContractExists(contractId);

        User vendor = userRepository.getOne(vendorId);
        Contract contract = contractRepository.findByIdAndVendor(contractId, vendor);

        long contractAmount = contract.getAmount();
        long invoiceAmount = request.getAmount();
        long invoicedContractAmount = invoiceRepository.getInvoicedContractAmount(contract);
        long pendingContractAmount = contractAmount - invoicedContractAmount;

        if (pendingContractAmount == 0) {
            throw new ContractPaidException("The contract " + contractId + " has already been paid");
        } else if (invoiceAmount > pendingContractAmount) {
            throw new ContractAmountExceededException(
                "The invoice amount exceeds the pending value of " + pendingContractAmount + " for the contract");
        }

        Invoice invoice = modelMapper.map(request, Invoice.class);
        invoice.setContract(contract);
        invoice = invoiceRepository.save(invoice);

        CreateInvoiceResponse response = new CreateInvoiceResponse();
        response.setId(invoice.getId());
        response.setPendingContractAmount(pendingContractAmount - invoiceAmount);

        return response;
    }

}
