package com.flashtract.contract.service.impl;

import com.flashtract.contract.enums.UserTypeEnum;
import com.flashtract.contract.exception.ClientUserNotFoundException;
import com.flashtract.contract.exception.ContractNotFoundException;
import com.flashtract.contract.exception.UserNotFoundException;
import com.flashtract.contract.exception.VendorUserNotFoundException;
import com.flashtract.contract.repository.ContractRepository;
import com.flashtract.contract.repository.UserRepository;
import com.flashtract.contract.service.IBusinessValidationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BusinessValidationServiceImpl implements IBusinessValidationService {

    private final UserRepository userRepository;
    private final ContractRepository contractRepository;

    @Override
    public void assertUserExists(String id) {
        if (!userRepository.existsById(id)) {
            throw new UserNotFoundException("User with id " + id + " not found");
        }
    }

    @Override
    public void assertClientUser(String id) {
        if (!userRepository.existsByIdAndType(id, UserTypeEnum.CLIENT)) {
            throw new ClientUserNotFoundException("Client user with id " + id + " not found");
        }
    }

    @Override
    public void assertVendorUser(String id) {
        if (!userRepository.existsByIdAndType(id, UserTypeEnum.VENDOR)) {
            throw new VendorUserNotFoundException("Vendor user with id " + id + " not found");
        }
    }

    @Override
    public void assertContractExists(String id) {
        if (!contractRepository.existsById(id)) {
            throw new ContractNotFoundException("Contract with id " + id + " not found");
        }
    }

}
