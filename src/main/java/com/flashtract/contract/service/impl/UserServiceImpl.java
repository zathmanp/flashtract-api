package com.flashtract.contract.service.impl;

import com.flashtract.contract.controller.request.CreateUserRequest;
import com.flashtract.contract.controller.response.CreateUserResponse;
import com.flashtract.contract.entity.User;
import com.flashtract.contract.repository.UserRepository;
import com.flashtract.contract.service.IUserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements IUserService {

    private final ModelMapper modelMapper;
    private final UserRepository userRepository;

    @Override
    public CreateUserResponse create(CreateUserRequest request) {
        User user = modelMapper.map(request, User.class);
        user = userRepository.save(user);

        CreateUserResponse response = new CreateUserResponse();
        response.setId(user.getId());

        return response;
    }

}
