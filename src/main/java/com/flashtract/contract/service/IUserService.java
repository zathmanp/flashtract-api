package com.flashtract.contract.service;

import com.flashtract.contract.controller.request.CreateUserRequest;
import com.flashtract.contract.controller.response.CreateUserResponse;

public interface IUserService {

    /**
     * Creates a new user
     *
     * @param request User information
     * @return The id of the new user
     */
    CreateUserResponse create(CreateUserRequest request);

}
